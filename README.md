# machine_learning

Scripts from course "Wissensrepräsentation", Summer semester 2018, HTW Berlin

Basics of Machine Learning
1. Univariate Linear Regression
2. Mutltivariate Linear Regression
3. Logistic Regression
4. Regularization
5. Bias/Variance tradeoff
6. Classification - Titanic train set
